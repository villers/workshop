// TODO faire un fichier commun kustomize
// TODO faire un fichier spécifique pour gitlab
## Usage avec kustomize

kustomize introduit une notion de surchage via des descripteurs qui sont dans un chemin. Tous les env sont décrits via cette notion de surchage. Il n'y a pas de templating mais l'on peut pour certains besoin spécifique (version des images, injection de secret, ...) avoir des `generator` qui vont faire cela (pas abordé ici).

Ces `overlays` sont gérés dans les applications tout simplement en utilisant le `spec.source.path`, par exemple

```yaml
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: hello-world
  namespace: argocd
spec:
  project: default 
  destination:
    namespace: test
    server: https://kubernetes.default.svc
  source:
    path: 02-kustomize/overlays/demo
    repoURL: https://github.com/louiznk/deploy-sith.git
    targetRevision: main
  syncPolicy:
    automated:
      prune: true
      selfHeal: true 
    syncOptions:
    - CreateNamespace=true 
```

**WARNING**
Ne pas gérer les différences entre environnements via les branches, c'est un antipattern