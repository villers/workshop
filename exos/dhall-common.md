## Usage avec Dhall

Dhall est vue comme un langage de programmation qui génére de fichiers (json, yaml ou toml).  
C'est un langage de programmation typé avec une approche fonctionnel. Cela permet d'avoir un fonctionnement plus sûr (typage, validation à la compilation, réutilisation, ...) mais plus complexe à mettre en oeuvre.  
Dhall est un système de configuration générique, il n'est pas spécialisé pour kubernetes. Cependant la communauté met fournit les typages spécifique à kubernetes ce qui permet de manipuler des objets kubernetes typé donc à la fois sûr (validation à la compilation) et avec un retour sur ce qui n'est pas conforme (retour de la compilation encore une fois).

Nous utiliserons dhall en faisant du templating (un peu comme helm) avec des fichiers de valeurs de chaque environnements. Contrairement à helm il n'y a pas d'organisation standardisé des fichiers, nous vous en proposant une qui à un mérite : elle marche pour le TP. Mais rassurez vous pour le TP vous n'avez pas besoin de devenir expert en dhall car vous serez assister d'un expert (moyennant finance) qui vous aidera dans tout cela.

// TODO : voir comment mettre le coût du consultant Beornide (10 po)

Vous trouverez dans le repository des descripteurs à déployer les modèles prets à emploie pour dhall dans le dossier 02-dhall (https://github.com/louiznk/deploy-sith/tree/main/02-dhall)

L'organisation des fichiers est la suivante
```
├── assembly-prod.dhall
├── assembly-secret.dhall
├── assembly-....dhall
├── configs
│   ├── prod.dhall
│   ├── staging.dhall
│   └── ....dhall
├── templates
│   ├── sith-all-template.dhall
│   ├── sith-deploy-template.dhall
│   ├── sith-ingress-template.dhall
│   └── sith-svc-template.dhall
├── tools
│   └── converters.dhall
└── types
    ├── sith-config-deploy.dhall
    ├── sith-config.dhall
    ├── sith-config-ingress.dhall
    ├── sith-config-ingress-rule.dhall
    ├── ...
    ├── sith-config-svc.dhall
    └── unionType.dhall
```

Vous avez à la racine les fichiers de composition de chaque environnements, ces fichiers charges les valeurs propres aux environnements et les templates qui sont génériques. Pour chaque environnement il y a donc un "lanceur", une configuration spécifique et l'appel au système de templating qui est générique.

Par exemple le fichier assembly-staging charge le fichier d'environnement `configs/staging.dhall` et utilise le templating commun. Si vous voulez changer la configuration d'un environnement, par exemple changer la version de l'image de l'environnement staging il vous suffit de changer dans le fichier de configuration de l'environnement staging (configs/staging.dhall) de modifier la valeur en ligne 26 (`image = "registry.gitlab.com/gitops-heros/sith:1.0",`)


Pour générer les manifests d'un environnement, par exemple staging il vous suffit de passer la commande
`dhall-to-yaml --documents --file assembly-staging.dhall`

Les templates et différents fichiers des environnements sont déjà disponible, vous allez devoir intégrer dhall dans votre système de deploiement et ensuite deployé et parfois de mettre à jours les configurations des environnements pour mettre à jours vous deployements.
							