# Contexte
On retrouve plusieurs notions dans Argo CD mais la principale est la notion d'Application.

Une application est "group of Kubernetes resources as defined by a manifest. This is a Custom Resource Definition (CRD)." 
C'est un object qui contient :
- une référence vers où trouver les descripteurs kubernetes
- une référence vers où déployer cela
- et un peut de configuration

**WARNING**
Faite un clone du repo (https://github.com/louiznk/deploy-sith.git) contenant les descripteurs d'exemple et travaillez sur votre clone.

## Usage simple

On peut créer une apps soit via la ligne de commande, soit via un descripteur (à privilégier...).

exemple :
```shell
argocd app create hello-world --repo https://github.com/louiznk/deploy-sith.git --path 01-static-yaml --dest-server https://kubernetes.default.svc --sync-option CreateNamespace=true --dest-namespace test
```

<!> l'option `--sync-option CreateNamespace=true ` n'est pas fiable...
<!> il n'y a pas toutes les options sur la `syncPolicy`


Ou via un descripteur

```yaml
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: hello-world
  namespace: argocd
spec:
  project: default 
  destination:
    namespace: test
    server: https://kubernetes.default.svc
  source:
    path: 01-static-yaml
    repoURL: https://github.com/louiznk/deploy-sith.git
    targetRevision: main
  syncPolicy:
    automated:
      prune: true
      selfHeal: true 
    syncOptions:
    - CreateNamespace=true 

```


Cette apps va déployer dans le serveur où est déployer Argo CD (https://kubernetes.default.svc est l'url interne de l'API server) les manifest kubernetes qui sont dans le repository publique https://github.com/louiznk/deploy-sith.git (on peut gérer des repository public mais c'est une autre histoire), dans le repertoire `01-static-yaml` et sur la branche `main`
Elle doit aussi créer le namespace si il n'existe pas. Si l'on modifie l'application sans passer par une modification du repo, argo doit le remettre en état.
<!> Si l'application est supprimer les ressources sont elles aussi supprimées (à part le namespace, sauf si il fait partie des descripteurs).

**WARNING**
Ne pas gérer les différences entre environnements via les branches, c'est un antipattern

### Notes
Parmis les concepts plus avancé (qui ne seront pas abordés) il y a un qui il est interessant de regarder : l'Apps de Apps et les AppSet