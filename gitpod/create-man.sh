#/bin/bash
export DATE=$(date +%F)
export ORGA="ZenOps"
mkdir -p man1
for md in `ls ../../workshop-gitops-ui/docs/bibliotheque/*.md`
do
  filename=$(basename $md)
  export name="${filename%.*}"
  export NAME=$(echo $name | tr '[:lower:]' '[:upper:]')
  echo "generate man from $filename"
  # https://pandoc.org/MANUAL.html#variables
  cat meta.tmp.yaml | envsubst > "meta-${name}.yaml"
  pandoc --title-prefix=$name --metadata-file="meta-${name}.yaml" --standalone --to man "$md" --output "./man1/${name}.1"
  gzip "./man1/${name}.1"
  rm "meta-${name}.yaml"

done

